// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyC2qxKDOjLeWrz3llm9mM8B_HiFStTEkps',
        authDomain: 'scm-ex.firebaseapp.com',
        databaseURL: 'https://scm-ex.firebaseio.com',
        projectId: 'scm-ex',
        storageBucket: 'scm-ex.appspot.com',
        messagingSenderId: '947582699741'
    }
};
