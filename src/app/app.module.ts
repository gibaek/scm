import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {CategoryModule} from './category/category.module';
import {ProductModule} from './product/product.module';
import {ScmMainModule} from './scm-main/scm-main.module';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {SharedModule} from './shared/shared.module';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastModule, ToastOptions} from 'ng2-toastr';
import {CustomToastOptions} from './custom-toast-options';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        /* Angular Modules*/
        BrowserModule,
        HttpModule,
        FormsModule,
        BrowserAnimationsModule,
        /* App Modules*/
        ScmMainModule,
        ProductModule,
        CategoryModule,
        AppRoutingModule,
        /* 3ed Modules*/
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        SharedModule,
        ToastModule.forRoot(),
        NgbPaginationModule.forRoot()
    ],
    providers: [{provide: ToastOptions, useClass: CustomToastOptions}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
