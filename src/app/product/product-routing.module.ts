import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductManagementComponent} from './product-management/product-management.component';
import {ProductDetailResolverService} from './product-detail/product-detail-resolver.service';
import {ProductDetailComponent} from './product-detail/product-detail.component';

const routes: Routes = [
    {
        path: 'product-list', children: [
        {path: '', pathMatch: 'full', component: ProductManagementComponent},
        {
            path: 'product/:no', resolve: {detail: ProductDetailResolverService},
            component: ProductDetailComponent
        }
    ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: [ProductDetailResolverService]
})
export class ProductRoutingModule {
}
