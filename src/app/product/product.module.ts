import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductManagementComponent} from './product-management/product-management.component';
import {ProductRoutingModule} from './product-routing.module';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProductListComponent } from './product-management/product-list/product-list.component';
import {PROD_LIST_PAGE_SIZE} from './products.tokens';
import { ProductStatusPipe } from './product-status.pipe';
import {CheckedProductSetService} from './product-management/checked-product-set.service';

@NgModule({
    imports: [
        CommonModule,
        ProductRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [ProductManagementComponent, ProductDetailComponent, ProductListComponent, ProductStatusPipe],
    providers: [ CheckedProductSetService, {provide: PROD_LIST_PAGE_SIZE, useValue: 6}]
})
export class ProductModule {
}
