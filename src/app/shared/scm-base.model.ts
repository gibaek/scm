export class ScmBase {
    constructor (isUse: boolean, createdTime: string, updatedTime: string) {
        this.isUse = isUse;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    isUse: boolean;
    createdTime: string;
    updatedTime: string;
}
