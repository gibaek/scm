import { Injectable } from '@angular/core';
import {DataStoreService} from '../../shared/data-store.service';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import * as firebase from 'firebase/app';

@Injectable()
export class CategoryDetailResolverService {

  constructor(private  database: DataStoreService, private  router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (route.queryParams['action'] === 'create') {
          return null;
      } else {
          return this.database.findObjectSnapshot('category', route.params['no']).map((snapshot: firebase.database.DataSnapshot) => {
              if (snapshot.exists()) {
                  return snapshot.val();
              }

              this.router.navigate(['/category-list']);
              return null;
          });
      }
  }
}
