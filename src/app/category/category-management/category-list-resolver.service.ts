import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {DataStoreService} from '../../shared/data-store.service';
import {CAT_LIST_PAGE_SIZE} from '../category.tokens';
import {Categories} from '../category.model';

@Injectable()
export class CategoryListResolverService implements Resolve<any>　{
    resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.database.count('category')
            .switchMap(cnt => this.database.findList$ByPage('category', 1, this.pageSize, cnt))
            .do((list: Categories) => list.sort((p1, p2) => p2.no - p1.no))
            .take(1);
    }

  constructor(private database: DataStoreService, @Inject(CAT_LIST_PAGE_SIZE) private pageSize: number) { }

}
